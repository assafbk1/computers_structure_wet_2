//
// Created by assaf on 12/4/19.
//

#include "cache.h"


uint32_t create_mask(unsigned mask_size) {
    return uint32_t(pow(2,mask_size)-1);
}
