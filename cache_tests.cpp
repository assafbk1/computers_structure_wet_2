//
// Created by assaf on 12/6/19.
//

#include <cassert>
#include "cache_tests.h"


using std::cout;
using std::endl;




void test_create_lru_fsm() {

    uint32_t num_of_ways = 4;
    LruFsm lrufsm = LruFsm(num_of_ways);

//    assert(lrufsm.ways_order_list.empty());
    assert(lrufsm.num_of_ways == num_of_ways);

}

void test_create_way_table() {

    uint32_t num_of_entries = 64;
    Way way_table = Way(num_of_entries);

    assert(way_table.entries[0]->is_valid == false);
    assert(way_table.entries[0]->tag == NO_TAG);
    assert(way_table.entries[num_of_entries-1]->is_valid == false);
    assert(way_table.entries[num_of_entries-1]->tag == NO_TAG);
    assert(way_table.num_of_entries == num_of_entries);

}

void test_create_cache() {

    uint32_t log_2_cache_size = 16;
    uint32_t log_2_block_size = 5;
    uint32_t log_2_ways = 3;
    uint32_t num_of_ways = 8;
    uint32_t num_of_entries = 256;
    uint32_t cycles = 1;
    bool is_write_allocate = true;

    Cache cache = Cache(log_2_cache_size, log_2_block_size, log_2_ways, is_write_allocate, cycles, nullptr);

    assert(cache.ways[0]->entries[0]->tag == NO_TAG);
    assert(cache.ways[num_of_ways-1]->entries[num_of_entries-1]->tag == NO_TAG);

    assert(cache.lru_fsms[0]->num_of_ways == num_of_ways);
    assert(cache.lru_fsms[num_of_entries-1]->num_of_ways == num_of_ways);

    assert(cache.is_write_allocate);

}

void test_LRU_update(){

    LruFsm lru(8);
    int i=0;
    auto it = lru.ways_order_list.begin();
    for(;it!=lru.ways_order_list.end();it++){
        assert(*it == i); // test init
        i++;
    }

    // test update:
    uint32_t last = lru.update(5);
    assert(last == 7);
    assert(lru.ways_order_list.front() == 5);
    it = lru.ways_order_list.begin();
    /*for(;it!=lru.ways_order_list.end();it++){
        std::cout << *it << " " ;
    }
    std::cout << std::endl;
*/
    last = lru.update(1);
    assert(last == 7);
    assert(lru.ways_order_list.front() == 1);
    it = lru.ways_order_list.begin();
    /*for(;it!=lru.ways_order_list.end();it++){
        std::cout << *it << " " ;
    }
    std::cout << std::endl;
*/
    last = lru.update(7);
    assert(last == 6);
    assert(lru.ways_order_list.front() == 7);
    it = lru.ways_order_list.begin();
  /*  for(;it!=lru.ways_order_list.end();it++){
        std::cout << *it << " " ;
    }
    std::cout << std::endl;
*/
}

void test_read(){

    Cache cache(16,4,3,true,1,nullptr);
    assert(!cache.cache_read(536419496));
    assert(cache.cache_read(536419496));
    assert(!cache.cache_read(0));
    assert(cache.cache_read(0));
    assert(!cache.cache_read(123456789));
    assert(cache.cache_read(123456789));

    Cache cache1(4,2,1,true,1,nullptr); // #entries = (2^4 / 2^2) / 2 = 2
    // #offset_bits = log2(block_size) = 2
    // # set_bits = log2(#entries) = 1
    uint32_t address1 = 4294967295;
    assert(!cache1.cache_read(address1));
    uint32_t address2 = 4294967291;
    assert(!cache1.cache_read(address2));

    assert(cache1.ways[1]->entries[0]->tag == 536870911);
    assert(cache1.ways[1]->entries[1]->tag == 536870911);

    assert(!cache1.cache_read(3154116475));
    assert(!cache1.cache_read(3154116479));

    assert(cache1.ways[0]->entries[0]->tag == 394264559);
    assert(cache1.ways[0]->entries[1]->tag == 394264559);

    assert(!cache1.cache_read(3154114539));
    assert(cache1.ways[1]->entries[0]->tag == 394264317);



}

void test_read_miss_as_l1_1() {   // not all ways are occupied in L1 and L2
                                  // L1 does not have the wanted variable
                                  // L2 does not have the wanted variable
    uint32_t log_2_cache_size = 16;
    uint32_t log_2_block_size = 3;
    uint32_t log_2_ways = 2;
    uint32_t cycles = 1;
    bool is_write_allocate = true;

    Cache cache_l2 = Cache(log_2_cache_size, log_2_block_size, log_2_ways, is_write_allocate, cycles, nullptr);
    Cache cache_l1 = Cache(log_2_cache_size, log_2_block_size, log_2_ways, is_write_allocate, cycles, &cache_l2);

    uint32_t tag = 0;
    uint32_t address = 0;
    // we assume the current position of entry 0's lru_fsm is correct
    cache_l1.ways[0]->entries[0]->tag = 12387;
    cache_l1.ways[0]->entries[0]->is_valid = true;

    cache_l2.ways[0]->entries[0]->tag = 12387;
    cache_l2.ways[0]->entries[0]->is_valid = true;

    cache_l1.cache_read(address);

    assert(cache_l1.ways[0]->entries[0]->tag == 12387);
    assert(cache_l1.ways[1]->entries[0]->tag == 0);
    assert(cache_l1.ways[1]->entries[0]->is_valid == false);
    assert(cache_l1.ways[2]->entries[0]->tag == 0);
    assert(cache_l1.ways[2]->entries[0]->is_valid == false);
    assert(cache_l1.ways[3]->entries[0]->tag == tag);
    assert(cache_l1.ways[3]->entries[0]->is_valid == true);  // he is supposed to be the lru in L1

    assert(cache_l2.ways[0]->entries[0]->tag == 12387);
    assert(cache_l2.ways[0]->entries[0]->is_valid == true);
    assert(cache_l2.ways[1]->entries[0]->tag == 0);
    assert(cache_l2.ways[1]->entries[0]->is_valid == false);
    assert(cache_l2.ways[2]->entries[0]->tag == 0);
    assert(cache_l2.ways[2]->entries[0]->is_valid == false);
    assert(cache_l2.ways[3]->entries[0]->tag == tag);       // he is supposed to be the lru in L2
    assert(cache_l2.ways[3]->entries[0]->is_valid == true);

    // checks that the lru fsm updated
    assert(cache_l1.lru_fsms[0]->ways_order_list.front() == 3);
    assert(cache_l1.lru_fsms[0]->ways_order_list.back() == 2);

    assert(cache_l2.lru_fsms[0]->ways_order_list.front() == 3);
    assert(cache_l2.lru_fsms[0]->ways_order_list.back() == 2);

    assert(cache_l1.total_misses == 1);
    assert(cache_l1.total_accesses == 1);
    assert(cache_l2.total_misses == 1);
    assert(cache_l2.total_accesses == 1);

}

void test_read_miss_as_l1_2() {   // not all ways are occupied in L1 and L2
                                  // L1 does not have the wanted variable
                                  // L2 does has the wanted variable
    uint32_t log_2_cache_size = 16;
    uint32_t log_2_block_size = 3;
    uint32_t log_2_ways = 2;
    uint32_t cycles = 1;
    bool is_write_allocate = true;

    Cache cache_l2 = Cache(log_2_cache_size, log_2_block_size, log_2_ways, is_write_allocate, cycles, nullptr);
    Cache cache_l1 = Cache(log_2_cache_size, log_2_block_size, log_2_ways, is_write_allocate, cycles, &cache_l2);

    uint32_t tag = 0;
    uint32_t address = 0;
    // we assume the current position of entry 0's lru_fsm is correct
    cache_l1.ways[0]->entries[0]->tag = 12387;
    cache_l1.ways[0]->entries[0]->is_valid = true;

    cache_l2.ways[0]->entries[0]->tag = 12387;
    cache_l2.ways[0]->entries[0]->is_valid = true;
    cache_l2.ways[3]->entries[0]->tag = tag;
    cache_l2.ways[3]->entries[0]->is_valid = true;


    cache_l1.cache_read(address);

    assert(cache_l1.ways[0]->entries[0]->tag == 12387);
    assert(cache_l1.ways[1]->entries[0]->tag == 0);
    assert(cache_l1.ways[1]->entries[0]->is_valid == false);
    assert(cache_l1.ways[2]->entries[0]->tag == 0);
    assert(cache_l1.ways[2]->entries[0]->is_valid == false);
    assert(cache_l1.ways[3]->entries[0]->tag == tag);
    assert(cache_l1.ways[3]->entries[0]->is_valid == true);  // he is supposed to be the lru in L1

    assert(cache_l2.ways[0]->entries[0]->tag == 12387);
    assert(cache_l2.ways[0]->entries[0]->is_valid == true);
    assert(cache_l2.ways[1]->entries[0]->tag == 0);
    assert(cache_l2.ways[1]->entries[0]->is_valid == false);
    assert(cache_l2.ways[2]->entries[0]->tag == 0);
    assert(cache_l2.ways[2]->entries[0]->is_valid == false);
    assert(cache_l2.ways[3]->entries[0]->tag == tag);       // he is supposed to be the lru in L2
    assert(cache_l2.ways[3]->entries[0]->is_valid == true);

    // checks that the lru fsm updated
    assert(cache_l1.lru_fsms[0]->ways_order_list.front() == 3);
    assert(cache_l1.lru_fsms[0]->ways_order_list.back() == 2);
    assert(cache_l2.lru_fsms[0]->ways_order_list.front() == 3);
    assert(cache_l2.lru_fsms[0]->ways_order_list.back() == 2);

    assert(cache_l1.total_misses == 1);
    assert(cache_l1.total_accesses == 1);
    assert(cache_l2.total_misses == 0);
    assert(cache_l2.total_accesses == 1);
}

void test_read_miss_as_l1_3() {   // all ways are occupied in L1 and L2
                                  // L1 does not have the wanted variable
                                  // L2 does not have the wanted variable
    uint32_t log_2_cache_size = 16;
    uint32_t log_2_block_size = 3;
    uint32_t log_2_ways = 2;
    uint32_t cycles = 1;
    bool is_write_allocate = true;

    Cache cache_l2 = Cache(log_2_cache_size, log_2_block_size, log_2_ways, is_write_allocate, cycles, nullptr);
    Cache cache_l1 = Cache(log_2_cache_size, log_2_block_size, log_2_ways, is_write_allocate, cycles, &cache_l2);

    uint32_t tag = 0;
    uint32_t address = 0;
    // we assume the current position of entry 0's lru_fsm is correct
    cache_l1.ways[0]->entries[0]->tag = 12387;
    cache_l1.ways[0]->entries[0]->is_valid = true;
    cache_l1.ways[1]->entries[0]->tag = 12387;
    cache_l1.ways[1]->entries[0]->is_valid = true;
    cache_l1.ways[2]->entries[0]->tag = 12387;
    cache_l1.ways[2]->entries[0]->is_valid = true;
    cache_l1.ways[3]->entries[0]->tag = 12387;
    cache_l1.ways[3]->entries[0]->is_valid = true;

    cache_l2.ways[0]->entries[0]->tag = 12387;
    cache_l2.ways[0]->entries[0]->is_valid = true;
    cache_l2.ways[1]->entries[0]->tag = 12387;
    cache_l2.ways[1]->entries[0]->is_valid = true;
    cache_l2.ways[2]->entries[0]->tag = 12387;
    cache_l2.ways[2]->entries[0]->is_valid = true;
    cache_l2.ways[3]->entries[0]->tag = 12387;
    cache_l2.ways[3]->entries[0]->is_valid = true;

    cache_l1.cache_read(address);

    assert(cache_l1.ways[0]->entries[0]->tag == 12387);
    assert(cache_l1.ways[1]->entries[0]->tag == 12387);
    assert(cache_l1.ways[2]->entries[0]->tag == 12387);
    assert(cache_l1.ways[3]->entries[0]->tag == tag);   // he is supposed to be the lru in L1

    assert(cache_l2.ways[0]->entries[0]->tag == 12387);
    assert(cache_l2.ways[1]->entries[0]->tag == 12387);
    assert(cache_l2.ways[2]->entries[0]->tag == 12387);
    assert(cache_l2.ways[3]->entries[0]->tag == tag);   // he is supposed to be the lru in L1

    // checks that the lru fsm updated
    assert(cache_l1.lru_fsms[0]->ways_order_list.front() == 3);
    assert(cache_l1.lru_fsms[0]->ways_order_list.back() == 2);
    assert(cache_l2.lru_fsms[0]->ways_order_list.front() == 3);
    assert(cache_l2.lru_fsms[0]->ways_order_list.back() == 2);

    assert(cache_l1.total_misses == 1);
    assert(cache_l1.total_accesses == 1);
    assert(cache_l2.total_misses == 1);
    assert(cache_l2.total_accesses == 1);

}

void test_read_miss_as_l1_4() {   // all ways are occupied in L1 and L2
                                  // L1 does not have the wanted variable
                                  // L2 does has the wanted variable
    uint32_t log_2_cache_size = 16;
    uint32_t log_2_block_size = 3;
    uint32_t log_2_ways = 2;
    uint32_t cycles = 1;
    bool is_write_allocate = true;

    Cache cache_l2 = Cache(log_2_cache_size, log_2_block_size, log_2_ways, is_write_allocate, cycles, nullptr);
    Cache cache_l1 = Cache(log_2_cache_size, log_2_block_size, log_2_ways, is_write_allocate, cycles, &cache_l2);

    uint32_t tag = 0;
    uint32_t address = 0;
    // we assume the current position of entry 0's lru_fsm is correct
    cache_l1.ways[0]->entries[0]->tag = 12387;
    cache_l1.ways[0]->entries[0]->is_valid = true;
    cache_l1.ways[1]->entries[0]->tag = 12387;
    cache_l1.ways[1]->entries[0]->is_valid = true;
    cache_l1.ways[2]->entries[0]->tag = 12387;
    cache_l1.ways[2]->entries[0]->is_valid = true;
    cache_l1.ways[3]->entries[0]->tag = 12387;
    cache_l1.ways[3]->entries[0]->is_valid = true;

    cache_l2.ways[0]->entries[0]->tag = 12387;
    cache_l2.ways[0]->entries[0]->is_valid = true;
    cache_l2.ways[1]->entries[0]->tag = tag;
    cache_l2.ways[1]->entries[0]->is_valid = true;
    cache_l2.ways[2]->entries[0]->tag = 12387;
    cache_l2.ways[2]->entries[0]->is_valid = true;
    cache_l2.ways[3]->entries[0]->tag = 12387;
    cache_l2.ways[3]->entries[0]->is_valid = true;

    cache_l1.cache_read(address);

    assert(cache_l1.ways[0]->entries[0]->tag == 12387);
    assert(cache_l1.ways[1]->entries[0]->tag == 12387);
    assert(cache_l1.ways[2]->entries[0]->tag == 12387);
    assert(cache_l1.ways[3]->entries[0]->tag == tag);   // he is supposed to be the lru in L1

    assert(cache_l2.ways[0]->entries[0]->tag == 12387);
    assert(cache_l2.ways[1]->entries[0]->tag == tag);
    assert(cache_l2.ways[2]->entries[0]->tag == 12387);
    assert(cache_l2.ways[3]->entries[0]->tag == 12387);   // he is supposed to be the lru in L1

    // checks that the lru fsm updated
    assert(cache_l1.lru_fsms[0]->ways_order_list.front() == 3);
    assert(cache_l1.lru_fsms[0]->ways_order_list.back() == 2);
    assert(cache_l2.lru_fsms[0]->ways_order_list.front() == 1);
    assert(cache_l2.lru_fsms[0]->ways_order_list.back() == 3);

    assert(cache_l1.total_misses == 1);
    assert(cache_l1.total_accesses == 1);
    assert(cache_l2.total_misses == 0);
    assert(cache_l2.total_accesses == 1);
    assert(cache_l2.total_main_memory_accesses == 0);
}

void test_read_miss_as_l1_5() {   // all ways are occupied in L1 and L2
                                  // L1 lru is dirty
                                  // L1 does not have the wanted variable
                                  // L2 does not have the wanted variable
    uint32_t log_2_cache_size = 16;
    uint32_t log_2_block_size = 3;
    uint32_t log_2_ways = 2;
    uint32_t cycles = 1;
    bool is_write_allocate = true;

    Cache cache_l2 = Cache(log_2_cache_size, log_2_block_size, log_2_ways, is_write_allocate, cycles, nullptr);
    Cache cache_l1 = Cache(log_2_cache_size, log_2_block_size, log_2_ways, is_write_allocate, cycles, &cache_l2);

    uint32_t tag = 0;
    uint32_t address = 0;
    // we assume the current position of entry 0's lru_fsm is correct
    cache_l1.ways[0]->entries[0]->tag = 12387;
    cache_l1.ways[0]->entries[0]->is_valid = true;
    cache_l1.ways[1]->entries[0]->tag = 12387;
    cache_l1.ways[1]->entries[0]->is_valid = true;
    cache_l1.ways[2]->entries[0]->tag = 12387;
    cache_l1.ways[2]->entries[0]->is_valid = true;
    cache_l1.ways[3]->entries[0]->tag = 38723;  // this one will be evicted
    cache_l1.ways[3]->entries[0]->is_valid = true;
    cache_l1.ways[3]->entries[0]->is_dirty = true;

    cache_l2.ways[0]->entries[0]->tag = 12387;
    cache_l2.ways[0]->entries[0]->is_valid = true;
    cache_l2.ways[1]->entries[0]->tag = 12387;
    cache_l2.ways[1]->entries[0]->is_valid = true;
    cache_l2.ways[2]->entries[0]->tag = 38723;  // this should be updated
    cache_l2.ways[2]->entries[0]->is_valid = true;
    cache_l2.ways[3]->entries[0]->tag = 12387;
    cache_l2.ways[3]->entries[0]->is_valid = true;

    cache_l1.cache_read(address);

    assert(cache_l1.ways[0]->entries[0]->tag == 12387);
    assert(cache_l1.ways[1]->entries[0]->tag == 12387);
    assert(cache_l1.ways[2]->entries[0]->tag == 12387);
    assert(cache_l1.ways[3]->entries[0]->tag == tag);   // he is supposed to be the lru in L1

    assert(cache_l2.ways[0]->entries[0]->tag == 12387);
    assert(cache_l2.ways[1]->entries[0]->tag == 12387);
    assert(cache_l2.ways[2]->entries[0]->tag == 38723);
    assert(cache_l2.ways[3]->entries[0]->tag == tag);   // he is supposed to be the lru in L1

    // checks that the lru fsm updated
    assert(cache_l1.lru_fsms[0]->ways_order_list.front() == 3);
    assert(cache_l1.lru_fsms[0]->ways_order_list.back() == 2);
    assert(cache_l2.lru_fsms[0]->ways_order_list.front() == 3);
    assert(cache_l2.lru_fsms[0]->ways_order_list.back() == 2);

    assert(cache_l1.total_misses == 1);
    assert(cache_l1.total_accesses == 1);
    assert(cache_l2.total_misses == 1);
    assert(cache_l2.total_accesses == 1);

}

void test_write_hit_1() {   // not all ways are occupied

    uint32_t log_2_cache_size = 16;
    uint32_t log_2_block_size = 3;
    uint32_t log_2_ways = 2;
    uint32_t cycles = 1;
    bool is_write_allocate = true;

    Cache cache = Cache(log_2_cache_size, log_2_block_size, log_2_ways, is_write_allocate, cycles, nullptr);

    uint32_t address = 0;
    // we assume the current position of entry 0's lru_fsm is correct
    cache.ways[0]->entries[0]->tag = address;
    cache.ways[0]->entries[0]->is_valid = true;

    cache.cache_write(address);

    assert(cache.ways[0]->entries[0]->tag == address);
    assert(cache.ways[0]->entries[0]->is_valid == true);

    // checks that the lru fsm's state is correct
    assert(cache.lru_fsms[0]->ways_order_list.front() == 0);
    assert(cache.lru_fsms[0]->ways_order_list.back() == 3);

    assert(cache.total_misses == 0);
    assert(cache.total_accesses == 1);

}

void test_write_hit_2() {  // not all ways are occupied

    uint32_t log_2_cache_size = 16;
    uint32_t log_2_block_size = 3;
    uint32_t log_2_ways = 2;
    uint32_t cycles = 1;
    bool is_write_allocate = true;

    Cache cache = Cache(log_2_cache_size, log_2_block_size, log_2_ways, is_write_allocate, cycles, nullptr);

    uint32_t address = 0;
    // we assume the current position of entry 0's lru_fsm is correct
    cache.ways[0]->entries[0]->tag = 12387;
    cache.ways[0]->entries[0]->is_valid = true;
    cache.ways[1]->entries[0]->tag = address;
    cache.ways[1]->entries[0]->is_valid = true;

    cache.cache_write(address);

    assert(cache.ways[0]->entries[0]->tag == 12387);
    assert(cache.ways[1]->entries[0]->tag == address);
    assert(cache.ways[0]->entries[0]->is_valid == true);

    // checks that the lru fsm's state is correct
    assert(cache.lru_fsms[0]->ways_order_list.front() == 1);
    assert(cache.lru_fsms[0]->ways_order_list.back() == 3);

    assert(cache.total_misses == 0);
    assert(cache.total_accesses == 1);

}

void test_write_hit_3() {  // all ways are occupied

    uint32_t log_2_cache_size = 16;
    uint32_t log_2_block_size = 3;
    uint32_t log_2_ways = 2;
    uint32_t cycles = 1;
    bool is_write_allocate = true;

    Cache cache = Cache(log_2_cache_size, log_2_block_size, log_2_ways, is_write_allocate, cycles, nullptr);

    uint32_t address = 0;
    // we assume the current position of entry 0's lru_fsm is correct
    cache.ways[0]->entries[0]->tag = 12387;
    cache.ways[0]->entries[0]->is_valid = true;
    cache.ways[1]->entries[0]->tag = 12387;
    cache.ways[1]->entries[0]->is_valid = true;
    cache.ways[2]->entries[0]->tag = 12387;
    cache.ways[2]->entries[0]->is_valid = true;
    cache.ways[3]->entries[0]->tag = address;
    cache.ways[3]->entries[0]->is_valid = true;

    cache.cache_write(address);

    assert(cache.ways[0]->entries[0]->tag == 12387);
    assert(cache.ways[1]->entries[0]->tag == 12387);
    assert(cache.ways[2]->entries[0]->tag == 12387);
    assert(cache.ways[3]->entries[0]->tag == address);

    // checks that the lru fsm's state is correct
    assert(cache.lru_fsms[0]->ways_order_list.front() == 3);
    assert(cache.lru_fsms[0]->ways_order_list.back() == 2);

    assert(cache.total_misses == 0);
    assert(cache.total_accesses == 1);

}

void test_write_miss_with_write_no_allocate_as_llc_1() {  // not all ways are occupied
                                                          // we are currently the llc, so there is no need to pass on the write request to the next level cache

    uint32_t log_2_cache_size = 16;
    uint32_t log_2_block_size = 3;
    uint32_t log_2_ways = 2;
    uint32_t num_of_ways = 4;
    uint32_t cycles = 1;
    bool is_write_allocate = false;

    Cache cache = Cache(log_2_cache_size, log_2_block_size, log_2_ways, is_write_allocate, cycles, nullptr);

    uint32_t address = 0;
    // we assume the current position of entry 0's lru_fsm is correct
    cache.ways[0]->entries[0]->tag = 12387;
    cache.ways[0]->entries[0]->is_valid = true;

    cache.cache_write(address);

    assert(cache.ways[0]->entries[0]->tag == 12387);
    assert(cache.ways[1]->entries[0]->tag == 0);
    assert(cache.ways[1]->entries[0]->is_valid == false);
    assert(cache.ways[2]->entries[0]->tag == 0);
    assert(cache.ways[2]->entries[0]->is_valid == false);
    assert(cache.ways[3]->entries[0]->tag == 0);
    assert(cache.ways[3]->entries[0]->is_valid == false);

    // checks that the lru fsm didn't change
    assert(cache.lru_fsms[0]->ways_order_list.front() == 0);
    assert(cache.lru_fsms[0]->ways_order_list.back() == num_of_ways-1);

    assert(cache.total_misses == 1);
    assert(cache.total_accesses == 1);

}

void test_write_miss_with_write_no_allocate_as_llc_2() {  // all ways are occupied
                                                          // we are currently the llc, so there is no need to pass on the write request to the next level cache
    uint32_t log_2_cache_size = 16;
    uint32_t log_2_block_size = 3;
    uint32_t log_2_ways = 2;
    uint32_t num_of_ways = 4;
    uint32_t cycles = 1;
    bool is_write_allocate = false;

    Cache cache = Cache(log_2_cache_size, log_2_block_size, log_2_ways, is_write_allocate, cycles, nullptr);

    uint32_t address = 0;
    // we assume the current position of entry 0's lru_fsm is correct
    cache.ways[0]->entries[0]->tag = 12387;
    cache.ways[0]->entries[0]->is_valid = true;
    cache.ways[1]->entries[0]->tag = 12387;
    cache.ways[1]->entries[0]->is_valid = true;
    cache.ways[2]->entries[0]->tag = 12387;
    cache.ways[2]->entries[0]->is_valid = true;
    cache.ways[3]->entries[0]->tag = 12387;
    cache.ways[3]->entries[0]->is_valid = true;

    cache.cache_write(address);

    assert(cache.ways[0]->entries[0]->tag == 12387);
    assert(cache.ways[1]->entries[0]->tag == 12387);
    assert(cache.ways[2]->entries[0]->tag == 12387);
    assert(cache.ways[3]->entries[0]->tag == 12387);

    // checks that the lru fsm didn't change
    assert(cache.lru_fsms[0]->ways_order_list.front() == 0);
    assert(cache.lru_fsms[0]->ways_order_list.back() == num_of_ways-1);

    assert(cache.total_misses == 1);
    assert(cache.total_accesses == 1);

}

void test_write_miss_with_write_no_allocate_as_l1_1() {   // not all ways are occupied in L1 and L2
                                                          // L1 does not have the wanted variable
                                                          // L2 does not have the wanted variable
    uint32_t log_2_cache_size = 16;
    uint32_t log_2_block_size = 3;
    uint32_t log_2_ways = 2;
    uint32_t cycles = 1;
    bool is_write_allocate = false;

    Cache cache_l2 = Cache(log_2_cache_size, log_2_block_size, log_2_ways, is_write_allocate, cycles, nullptr);
    Cache cache_l1 = Cache(log_2_cache_size, log_2_block_size, log_2_ways, is_write_allocate, cycles, &cache_l2);

    uint32_t address = 0;
    // we assume the current position of entry 0's lru_fsm is correct
    cache_l1.ways[0]->entries[0]->tag = 12387;
    cache_l1.ways[0]->entries[0]->is_valid = true;

    cache_l2.ways[0]->entries[0]->tag = 12387;
    cache_l2.ways[0]->entries[0]->is_valid = true;

    cache_l1.cache_write(address);

    assert(cache_l1.ways[0]->entries[0]->tag == 12387);
    assert(cache_l1.ways[1]->entries[0]->tag == 0);
    assert(cache_l1.ways[1]->entries[0]->is_valid == false);
    assert(cache_l1.ways[2]->entries[0]->tag == 0);
    assert(cache_l1.ways[2]->entries[0]->is_valid == false);
    assert(cache_l1.ways[3]->entries[0]->tag == 0);
    assert(cache_l1.ways[3]->entries[0]->is_valid == false);

    assert(cache_l2.ways[0]->entries[0]->tag == 12387);
    assert(cache_l2.ways[0]->entries[0]->is_valid == true);
    assert(cache_l2.ways[1]->entries[0]->tag == 0);
    assert(cache_l2.ways[1]->entries[0]->is_valid == false);
    assert(cache_l2.ways[2]->entries[0]->tag == 0);
    assert(cache_l2.ways[2]->entries[0]->is_valid == false);
    assert(cache_l2.ways[3]->entries[0]->tag == 0);
    assert(cache_l2.ways[3]->entries[0]->is_valid == false);

    // checks that the lru fsm didn't change it's state
    assert(cache_l1.lru_fsms[0]->ways_order_list.front() == 0);
    assert(cache_l1.lru_fsms[0]->ways_order_list.back() == 3);

    assert(cache_l2.lru_fsms[0]->ways_order_list.front() == 0);
    assert(cache_l2.lru_fsms[0]->ways_order_list.back() == 3);

    assert(cache_l1.total_misses == 1);
    assert(cache_l1.total_accesses == 1);
    assert(cache_l2.total_misses == 1);
    assert(cache_l2.total_accesses == 1);

}

void test_write_miss_with_write_no_allocate_as_l1_2() {   // not all ways are occupied in L1 and L2
                                                          // L1 does not have the wanted variable
                                                          // L2 has the wanted variable
    uint32_t log_2_cache_size = 16;
    uint32_t log_2_block_size = 3;
    uint32_t log_2_ways = 2;
    uint32_t cycles = 1;
    bool is_write_allocate = false;

    Cache cache_l2 = Cache(log_2_cache_size, log_2_block_size, log_2_ways, is_write_allocate, cycles, nullptr);
    Cache cache_l1 = Cache(log_2_cache_size, log_2_block_size, log_2_ways, is_write_allocate, cycles, &cache_l2);

    uint32_t tag = 0;
    uint32_t address = 0;
    // we assume the current position of entry 0's lru_fsm is correct
    cache_l1.ways[0]->entries[0]->tag = 12387;
    cache_l1.ways[0]->entries[0]->is_valid = true;

    cache_l2.ways[0]->entries[0]->tag = 12387;
    cache_l2.ways[0]->entries[0]->is_valid = true;
    cache_l2.ways[2]->entries[0]->tag = tag;
    cache_l2.ways[2]->entries[0]->is_valid = true;

    cache_l1.cache_write(address);

    assert(cache_l1.ways[0]->entries[0]->tag == 12387);
    assert(cache_l1.ways[1]->entries[0]->tag == 0);
    assert(cache_l1.ways[1]->entries[0]->is_valid == false);
    assert(cache_l1.ways[2]->entries[0]->tag == 0);
    assert(cache_l1.ways[2]->entries[0]->is_valid == false);
    assert(cache_l1.ways[3]->entries[0]->tag == 0);
    assert(cache_l1.ways[3]->entries[0]->is_valid == false);

    assert(cache_l2.ways[0]->entries[0]->tag == 12387);
    assert(cache_l2.ways[0]->entries[0]->is_valid == true);
    assert(cache_l2.ways[1]->entries[0]->tag == 0);
    assert(cache_l2.ways[1]->entries[0]->is_valid == false);
    assert(cache_l2.ways[2]->entries[0]->tag == tag);
    assert(cache_l2.ways[2]->entries[0]->is_valid == true);
    assert(cache_l2.ways[3]->entries[0]->tag == 0);
    assert(cache_l2.ways[3]->entries[0]->is_valid == false);

    // checks that the lru fsm didn't change it's state
    assert(cache_l1.lru_fsms[0]->ways_order_list.front() == 0);
    assert(cache_l1.lru_fsms[0]->ways_order_list.back() == 3);

    assert(cache_l2.lru_fsms[0]->ways_order_list.front() == 2);
    assert(cache_l2.lru_fsms[0]->ways_order_list.back() == 3);

    assert(cache_l1.total_misses == 1);
    assert(cache_l1.total_accesses == 1);
    assert(cache_l2.total_misses == 0);
    assert(cache_l2.total_accesses == 1);

}

void test_write_miss_with_write_no_allocate_as_l1_3() {   // ways are occupied in L1 and L2
                                                          // L1 does not have the wanted variable
                                                          // L2 has the wanted variable
    uint32_t log_2_cache_size = 16;
    uint32_t log_2_block_size = 3;
    uint32_t log_2_ways = 2;
    uint32_t cycles = 1;
    bool is_write_allocate = false;

    Cache cache_l2 = Cache(log_2_cache_size, log_2_block_size, log_2_ways, is_write_allocate, cycles, nullptr);
    Cache cache_l1 = Cache(log_2_cache_size, log_2_block_size, log_2_ways, is_write_allocate, cycles, &cache_l2);

    uint32_t tag = 0;
    uint32_t address = 0;
    // we assume the current position of entry 0's lru_fsm is correct
    cache_l1.ways[0]->entries[0]->tag = 12387;
    cache_l1.ways[0]->entries[0]->is_valid = true;
    cache_l1.ways[1]->entries[0]->tag = 12387;
    cache_l1.ways[1]->entries[0]->is_valid = true;
    cache_l1.ways[2]->entries[0]->tag = 12387;
    cache_l1.ways[2]->entries[0]->is_valid = true;
    cache_l1.ways[3]->entries[0]->tag = 12387;
    cache_l1.ways[3]->entries[0]->is_valid = true;

    cache_l2.ways[0]->entries[0]->tag = 12387;
    cache_l2.ways[0]->entries[0]->is_valid = true;
    cache_l2.ways[1]->entries[0]->tag = 12387;
    cache_l2.ways[1]->entries[0]->is_valid = true;
    cache_l2.ways[2]->entries[0]->tag = tag;
    cache_l2.ways[2]->entries[0]->is_valid = true;
    cache_l2.ways[3]->entries[0]->tag = 12387;
    cache_l2.ways[3]->entries[0]->is_valid = true;

    cache_l1.cache_write(address);

    assert(cache_l1.ways[0]->entries[0]->tag == 12387);
    assert(cache_l1.ways[1]->entries[0]->tag == 12387);
    assert(cache_l1.ways[2]->entries[0]->tag == 12387);
    assert(cache_l1.ways[3]->entries[0]->tag == 12387);

    assert(cache_l2.ways[0]->entries[0]->tag == 12387);
    assert(cache_l2.ways[1]->entries[0]->tag == 12387);
    assert(cache_l2.ways[2]->entries[0]->tag == tag);
    assert(cache_l2.ways[3]->entries[0]->tag == 12387);

    // checks that the lru fsm didn't change it's state
    assert(cache_l1.lru_fsms[0]->ways_order_list.front() == 0);
    assert(cache_l1.lru_fsms[0]->ways_order_list.back() == 3);

    assert(cache_l2.lru_fsms[0]->ways_order_list.front() == 2);
    assert(cache_l2.lru_fsms[0]->ways_order_list.back() == 3);

    assert(cache_l1.total_misses == 1);
    assert(cache_l1.total_accesses == 1);
    assert(cache_l2.total_misses == 0);
    assert(cache_l2.total_accesses == 1);
}

// there is no point of testing this as llc, since there is never a write miss with write allocate in caches that are not l1
void test_write_miss_with_write_allocate_as_l1_1() {   // not all ways are occupied in L1 and L2
                                                       // L1 does not have the wanted variable
                                                       // L2 does not have the wanted variable
    uint32_t log_2_cache_size = 16;
    uint32_t log_2_block_size = 3;
    uint32_t log_2_ways = 2;
    uint32_t cycles = 1;
    bool is_write_allocate = true;

    Cache cache_l2 = Cache(log_2_cache_size, log_2_block_size, log_2_ways, is_write_allocate, cycles, nullptr);
    Cache cache_l1 = Cache(log_2_cache_size, log_2_block_size, log_2_ways, is_write_allocate, cycles, &cache_l2);

    uint32_t tag = 0;
    uint32_t address = 0;
    // we assume the current position of entry 0's lru_fsm is correct
    cache_l1.ways[0]->entries[0]->tag = 12387;
    cache_l1.ways[0]->entries[0]->is_valid = true;

    cache_l2.ways[0]->entries[0]->tag = 12387;
    cache_l2.ways[0]->entries[0]->is_valid = true;

    cache_l1.cache_write(address);

    assert(cache_l1.ways[0]->entries[0]->tag == 12387);
    assert(cache_l1.ways[1]->entries[0]->tag == 0);
    assert(cache_l1.ways[1]->entries[0]->is_valid == false);
    assert(cache_l1.ways[2]->entries[0]->tag == 0);
    assert(cache_l1.ways[2]->entries[0]->is_valid == false);
    assert(cache_l1.ways[3]->entries[0]->tag == tag);
    assert(cache_l1.ways[3]->entries[0]->is_valid == true);  // he is supposed to be the lru in L1

    assert(cache_l2.ways[0]->entries[0]->tag == 12387);
    assert(cache_l2.ways[0]->entries[0]->is_valid == true);
    assert(cache_l2.ways[1]->entries[0]->tag == 0);
    assert(cache_l2.ways[1]->entries[0]->is_valid == false);
    assert(cache_l2.ways[2]->entries[0]->tag == 0);
    assert(cache_l2.ways[2]->entries[0]->is_valid == false);
    assert(cache_l2.ways[3]->entries[0]->tag == tag);       // he is supposed to be the lru in L2
    assert(cache_l2.ways[3]->entries[0]->is_valid == true);

    // checks that the lru fsm updated
    assert(cache_l1.lru_fsms[0]->ways_order_list.front() == 3);
    assert(cache_l1.lru_fsms[0]->ways_order_list.back() == 2);

    assert(cache_l2.lru_fsms[0]->ways_order_list.front() == 3);
    assert(cache_l2.lru_fsms[0]->ways_order_list.back() == 2);

    assert(cache_l1.total_misses == 1);
    assert(cache_l1.total_accesses == 1);
    assert(cache_l2.total_misses == 1);
    assert(cache_l2.total_accesses == 1);

}

void test_write_miss_with_write_allocate_as_l1_2() {   // all ways are occupied in L1 and L2
                                                       // L1 does not have the wanted variable
                                                       // L2 does not have the wanted variable
    uint32_t log_2_cache_size = 16;
    uint32_t log_2_block_size = 3;
    uint32_t log_2_ways = 2;
    uint32_t cycles = 1;
    bool is_write_allocate = true;

    Cache cache_l2 = Cache(log_2_cache_size, log_2_block_size, log_2_ways, is_write_allocate, cycles, nullptr);
    Cache cache_l1 = Cache(log_2_cache_size, log_2_block_size, log_2_ways, is_write_allocate, cycles, &cache_l2);

    uint32_t tag = 0;
    uint32_t address = 0;
    // we assume the current position of entry 0's lru_fsm is correct
    cache_l1.ways[0]->entries[0]->tag = 12387;
    cache_l1.ways[0]->entries[0]->is_valid = true;
    cache_l1.ways[1]->entries[0]->tag = 12387;
    cache_l1.ways[1]->entries[0]->is_valid = true;
    cache_l1.ways[2]->entries[0]->tag = 12387;
    cache_l1.ways[2]->entries[0]->is_valid = true;
    cache_l1.ways[3]->entries[0]->tag = 12387;
    cache_l1.ways[3]->entries[0]->is_valid = true;

    cache_l2.ways[0]->entries[0]->tag = 12387;
    cache_l2.ways[0]->entries[0]->is_valid = true;
    cache_l2.ways[1]->entries[0]->tag = 12387;
    cache_l2.ways[1]->entries[0]->is_valid = true;
    cache_l2.ways[2]->entries[0]->tag = 12387;
    cache_l2.ways[2]->entries[0]->is_valid = true;
    cache_l2.ways[3]->entries[0]->tag = 12387;
    cache_l2.ways[3]->entries[0]->is_valid = true;

    cache_l1.cache_write(address);

    assert(cache_l1.ways[0]->entries[0]->tag == 12387);
    assert(cache_l1.ways[1]->entries[0]->tag == 12387);
    assert(cache_l1.ways[2]->entries[0]->tag == 12387);
    assert(cache_l1.ways[3]->entries[0]->tag == tag); // he is supposed to be the lru in L1

    assert(cache_l2.ways[0]->entries[0]->tag == 12387);
    assert(cache_l2.ways[1]->entries[0]->tag == 12387);
    assert(cache_l2.ways[2]->entries[0]->tag == 12387);
    assert(cache_l2.ways[3]->entries[0]->tag == tag);       // he is supposed to be the lru in L2

    // checks that the lru fsm updated
    assert(cache_l1.lru_fsms[0]->ways_order_list.front() == 3);
    assert(cache_l1.lru_fsms[0]->ways_order_list.back() == 2);

    assert(cache_l2.lru_fsms[0]->ways_order_list.front() == 3);
    assert(cache_l2.lru_fsms[0]->ways_order_list.back() == 2);

    assert(cache_l1.total_misses == 1);
    assert(cache_l1.total_accesses == 1);
    assert(cache_l2.total_misses == 1);
    assert(cache_l2.total_accesses == 1);

}

void test_write_miss_with_write_allocate_as_l1_3() {   // not all ways are occupied in L1
                                                       // L1 does not have the wanted variable
                                                       // L2 does have the wanted variable
    uint32_t log_2_cache_size = 16;
    uint32_t log_2_block_size = 3;
    uint32_t log_2_ways = 2;
    uint32_t cycles = 1;
    bool is_write_allocate = true;

    Cache cache_l2 = Cache(log_2_cache_size, log_2_block_size, log_2_ways, is_write_allocate, cycles, nullptr);
    Cache cache_l1 = Cache(log_2_cache_size, log_2_block_size, log_2_ways, is_write_allocate, cycles, &cache_l2);

    uint32_t tag = 0;
    uint32_t address = 0;
    // we assume the current position of entry 0's lru_fsm is correct
    cache_l1.ways[0]->entries[0]->tag = 12387;
    cache_l1.ways[0]->entries[0]->is_valid = true;

    cache_l2.ways[0]->entries[0]->tag = 12387;
    cache_l2.ways[0]->entries[0]->is_valid = true;
    cache_l2.ways[1]->entries[0]->tag = 12387;
    cache_l2.ways[1]->entries[0]->is_valid = true;
    cache_l2.ways[2]->entries[0]->tag = tag;
    cache_l2.ways[2]->entries[0]->is_valid = true;
    cache_l2.ways[3]->entries[0]->tag = 12387;
    cache_l2.ways[3]->entries[0]->is_valid = true;

    cache_l1.cache_write(address);

    assert(cache_l1.ways[0]->entries[0]->tag == 12387);
    assert(cache_l1.ways[1]->entries[0]->tag == 0);
    assert(cache_l1.ways[1]->entries[0]->is_valid == false);
    assert(cache_l1.ways[2]->entries[0]->tag == 0);
    assert(cache_l1.ways[2]->entries[0]->is_valid == false);
    assert(cache_l1.ways[3]->entries[0]->tag == tag);
    assert(cache_l1.ways[3]->entries[0]->is_valid == true);  // he is supposed to be the lru in L1

    assert(cache_l2.ways[0]->entries[0]->tag == 12387);
    assert(cache_l2.ways[1]->entries[0]->is_valid == true);
    assert(cache_l2.ways[1]->entries[0]->tag == 12387);
    assert(cache_l2.ways[1]->entries[0]->is_valid == true);
    assert(cache_l2.ways[2]->entries[0]->tag == tag);
    assert(cache_l2.ways[2]->entries[0]->is_valid == true);
    assert(cache_l2.ways[3]->entries[0]->tag == 12387);       // he is supposed to be the lru in L2
    assert(cache_l2.ways[3]->entries[0]->is_valid == true);

    // checks that the lru fsm updated
    assert(cache_l1.lru_fsms[0]->ways_order_list.front() == 3);
    assert(cache_l1.lru_fsms[0]->ways_order_list.back() == 2);

    assert(cache_l2.lru_fsms[0]->ways_order_list.front() == 2);
    assert(cache_l2.lru_fsms[0]->ways_order_list.back() == 3);

    assert(cache_l1.total_misses == 1);
    assert(cache_l1.total_accesses == 1);
    assert(cache_l2.total_misses == 0);
    assert(cache_l2.total_accesses == 1);

}

void test_write_statistics() {  // check statistics

    uint32_t log_2_cache_size = 16;
    uint32_t log_2_block_size = 3;
    uint32_t log_2_ways = 2;
    uint32_t cycles = 1;
    bool is_write_allocate = true;

    Cache cache_l2 = Cache(log_2_cache_size, log_2_block_size, log_2_ways, is_write_allocate, cycles, nullptr);
    Cache cache_l1 = Cache(log_2_cache_size, log_2_block_size, log_2_ways, is_write_allocate, cycles, &cache_l2);

    uint32_t address = 0;
    // we assume the current position of entry 0's lru_fsm is correct
    cache_l1.ways[0]->entries[0]->tag = 12387;
    cache_l1.ways[0]->entries[0]->is_valid = true;
    cache_l1.ways[1]->entries[0]->tag = 12387;
    cache_l1.ways[1]->entries[0]->is_valid = true;
    cache_l1.ways[2]->entries[0]->tag = 12387;
    cache_l1.ways[2]->entries[0]->is_valid = true;
    cache_l1.ways[3]->entries[0]->tag = 12387;
    cache_l1.ways[3]->entries[0]->is_valid = true;

    cache_l2.ways[0]->entries[0]->tag = 12387;
    cache_l2.ways[0]->entries[0]->is_valid = true;
    cache_l2.ways[1]->entries[0]->tag = 12387;
    cache_l2.ways[1]->entries[0]->is_valid = true;
    cache_l2.ways[2]->entries[0]->tag = 12387;
    cache_l2.ways[2]->entries[0]->is_valid = true;
    cache_l2.ways[3]->entries[0]->tag = 12387;
    cache_l2.ways[3]->entries[0]->is_valid = true;

    cache_l1.cache_write(address);     // w.miss
    cache_l1.cache_write(address+103); // w.miss
    cache_l1.cache_write(address+203); // w.miss
    cache_l1.cache_write(address);     // w.hit
    cache_l1.cache_write(address+103); // w.hit
    cache_l1.cache_write(address+203); // w.hit

    cache_l1.cache_read(address+304);  // r.miss
    cache_l1.cache_read(address+404);  // r.miss

    // generate read hit in L2 for read miss in L1
    cache_l2.ways[3]->entries[63]->tag = 0;
    cache_l2.ways[3]->entries[63]->is_valid = true;
    cache_l1.cache_read(address+504);  // r.hit

    cache_l1.cache_read(address+304);  // r.hit
    cache_l1.cache_read(address+404);  // r.hit
    cache_l1.cache_read(address+504);  // r.hit

    assert(cache_l1.total_misses == 6);
    assert(cache_l1.total_accesses == 12);
    assert(cache_l2.total_misses == 5);
    assert(cache_l2.total_accesses == 6);

}

void test_generate_lru_address() {

    uint32_t log_2_cache_size = 16;
    uint32_t log_2_block_size = 3; // 3 offset bits
    uint32_t log_2_ways = 2; // 3 offset bits
                             // 16-3-2 = 11 entry bits
                             // 32-11-3 = 18 tag bits
    uint32_t cycles = 1;
    bool is_write_allocate = true;

    Cache cache = Cache(log_2_cache_size, log_2_block_size, log_2_ways, is_write_allocate, cycles, nullptr);

    uint32_t tag = 255;
    uint32_t entry = 255;

    uint32_t address = cache.get_lru_address(tag, entry);
    assert(address == 4179960);
}

void test_lrufsm_invalidate() {

    uint32_t num_of_ways = 4;
    LruFsm lrufsm = LruFsm(num_of_ways);

    lrufsm.invalidate(1);
    assert(lrufsm.ways_order_list.front() == 0);
    assert(lrufsm.ways_order_list.back() == 1);

    lrufsm.invalidate(0);
    assert(lrufsm.ways_order_list.front() == 2);
    assert(lrufsm.ways_order_list.back() == 0);

    lrufsm.invalidate(3);
    assert(lrufsm.ways_order_list.front() == 2);
    assert(lrufsm.ways_order_list.back() == 3);

    lrufsm.invalidate(2);
    assert(lrufsm.ways_order_list.front() == 1);
    assert(lrufsm.ways_order_list.back() == 2);

}

void test_invalidate_block_1() { // entry is not full

    uint32_t log_2_cache_size = 16;
    uint32_t log_2_block_size = 3;
    uint32_t log_2_ways = 2;
    uint32_t cycles = 1;
    bool is_write_allocate = true;

    Cache cache = Cache(log_2_cache_size, log_2_block_size, log_2_ways, is_write_allocate, cycles, nullptr);

    uint32_t tag = 0;
    uint32_t address = 0;
    // we assume the current position of entry 0's lru_fsm is correct
    cache.ways[0]->entries[0]->tag = 12387;
    cache.ways[0]->entries[0]->is_valid = true;
    cache.ways[1]->entries[0]->tag = tag;
    cache.ways[1]->entries[0]->is_valid = true;
    cache.ways[1]->entries[0]->is_dirty = true;

    cache.invalidate_block(address);

    assert(cache.ways[0]->entries[0]->tag == 12387);
    assert(cache.ways[0]->entries[0]->is_valid == true);
    assert(cache.ways[1]->entries[0]->tag == tag);
    assert(cache.ways[1]->entries[0]->is_valid == false);
    assert(cache.ways[1]->entries[0]->is_dirty == false);
    assert(cache.ways[2]->entries[0]->tag == 0);
    assert(cache.ways[2]->entries[0]->is_valid == false);
    assert(cache.ways[3]->entries[0]->tag == 0);
    assert(cache.ways[3]->entries[0]->is_valid == false);


    // checks that the lru fsm updated
    assert(cache.lru_fsms[0]->ways_order_list.front() == 0);
    assert(cache.lru_fsms[0]->ways_order_list.back() == 1);

}

void test_invalidate_block_2() { // entry is full

    uint32_t log_2_cache_size = 16;
    uint32_t log_2_block_size = 3;
    uint32_t log_2_ways = 2;
    uint32_t cycles = 1;
    bool is_write_allocate = true;

    Cache cache = Cache(log_2_cache_size, log_2_block_size, log_2_ways, is_write_allocate, cycles, nullptr);

    uint32_t tag = 0;
    uint32_t address = 0;
    // we assume the current position of entry 0's lru_fsm is correct
    cache.ways[0]->entries[0]->tag = 12387;
    cache.ways[0]->entries[0]->is_valid = true;
    cache.ways[1]->entries[0]->tag = tag;
    cache.ways[1]->entries[0]->is_valid = true;
    cache.ways[1]->entries[0]->is_dirty = false;
    cache.ways[2]->entries[0]->tag = 12387;
    cache.ways[2]->entries[0]->is_valid = true;
    cache.ways[3]->entries[0]->tag = 12387;
    cache.ways[3]->entries[0]->is_valid = true;

    cache.invalidate_block(address);

    assert(cache.ways[0]->entries[0]->tag == 12387);
    assert(cache.ways[0]->entries[0]->is_valid == true);
    assert(cache.ways[1]->entries[0]->tag == tag);
    assert(cache.ways[1]->entries[0]->is_valid == false);
    assert(cache.ways[1]->entries[0]->is_dirty == false);
    assert(cache.ways[2]->entries[0]->tag == 12387);
    assert(cache.ways[2]->entries[0]->is_valid == true);
    assert(cache.ways[3]->entries[0]->tag == 12387);
    assert(cache.ways[3]->entries[0]->is_valid == true);


    // checks that the lru fsm updated
    assert(cache.lru_fsms[0]->ways_order_list.front() == 0);
    assert(cache.lru_fsms[0]->ways_order_list.back() == 1);

}

void test_invalidate_block_3() { // address is not in this cache
                                 // entry is full

    uint32_t log_2_cache_size = 16;
    uint32_t log_2_block_size = 3;
    uint32_t log_2_ways = 2;
    uint32_t cycles = 1;
    bool is_write_allocate = true;

    Cache cache = Cache(log_2_cache_size, log_2_block_size, log_2_ways, is_write_allocate, cycles, nullptr);

    uint32_t tag = 0;
    uint32_t address = 0;
    // we assume the current position of entry 0's lru_fsm is correct
    cache.ways[0]->entries[0]->tag = 12387;
    cache.ways[0]->entries[0]->is_valid = true;
    cache.ways[1]->entries[0]->tag = 12387;
    cache.ways[1]->entries[0]->is_valid = true;
    cache.ways[2]->entries[0]->tag = 12387;
    cache.ways[2]->entries[0]->is_valid = true;
    cache.ways[3]->entries[0]->tag = 12387;
    cache.ways[3]->entries[0]->is_valid = true;

    cache.invalidate_block(address);

    assert(cache.ways[0]->entries[0]->tag == 12387);
    assert(cache.ways[0]->entries[0]->is_valid == true);
    assert(cache.ways[1]->entries[0]->tag == 12387);
    assert(cache.ways[1]->entries[0]->is_valid == true);
    assert(cache.ways[2]->entries[0]->tag == 12387);
    assert(cache.ways[2]->entries[0]->is_valid == true);
    assert(cache.ways[3]->entries[0]->tag == 12387);
    assert(cache.ways[3]->entries[0]->is_valid == true);


    // checks that the lru fsm didnt change
    assert(cache.lru_fsms[0]->ways_order_list.front() == 0);
    assert(cache.lru_fsms[0]->ways_order_list.back() == 3);

}

void test_invalidate_block_4() { // address is not in this cache
                                 // entry is not full

    uint32_t log_2_cache_size = 16;
    uint32_t log_2_block_size = 3;
    uint32_t log_2_ways = 2;
    uint32_t cycles = 1;
    bool is_write_allocate = true;

    Cache cache = Cache(log_2_cache_size, log_2_block_size, log_2_ways, is_write_allocate, cycles, nullptr);

    uint32_t tag = 0;
    uint32_t address = 0;
    // we assume the current position of entry 0's lru_fsm is correct
    cache.ways[0]->entries[0]->tag = 12387;
    cache.ways[0]->entries[0]->is_valid = true;
    cache.ways[1]->entries[0]->tag = 12387;
    cache.ways[1]->entries[0]->is_valid = true;

    cache.invalidate_block(address);

    assert(cache.ways[0]->entries[0]->tag == 12387);
    assert(cache.ways[0]->entries[0]->is_valid == true);
    assert(cache.ways[1]->entries[0]->tag == 12387);
    assert(cache.ways[1]->entries[0]->is_valid == true);
    assert(cache.ways[2]->entries[0]->tag == 0);
    assert(cache.ways[2]->entries[0]->is_valid == false);
    assert(cache.ways[3]->entries[0]->tag == 0);
    assert(cache.ways[3]->entries[0]->is_valid == false);


    // checks that the lru fsm didnt change
    assert(cache.lru_fsms[0]->ways_order_list.front() == 0);
    assert(cache.lru_fsms[0]->ways_order_list.back() == 3);

}

void test_read_with_dirty(){

    Cache cache_l2(4,2,1,true,1,nullptr); // #entries = (2^4 / 2^2) / 2 = 2
    // #offset_bits = log2(block_size) = 2
    // # set_bits = log2(#entries) = 1

    Cache cache_l1(4,2,1,true,1,&cache_l2); // #entries = (2^4 / 2^2) / 2 = 2
    // #offset_bits = log2(block_size) = 2
    // # set_bits = log2(#entries) = 1

    cache_l2.set_upper_level_cache(&cache_l1);

    // fill L1 both ways of entry = 1:
    cache_l1.ways[1]->entries[1]->tag = 12;
    cache_l1.ways[1]->entries[1]->is_valid = true;
    cache_l1.ways[0]->entries[1]->tag = 40;
    cache_l1.ways[0]->entries[1]->is_valid = true;

    // way = 1 will be LRU - we set dirty = true there:
    cache_l1.ways[1]->entries[1]->is_dirty = true;

    // simulate read miss for L1 cache - should call L2 read - way = 1 is LRU and dirty = true =>  should write it to L2.
    cache_l1.cache_read(68); // entry = 1, tag = 8
    assert(cache_l1.ways[1]->entries[1]->tag == 8);
    assert(cache_l1.ways[1]->entries[1]->is_valid);
    // L2 assertions - the data should appear there according to the decisions tree
    assert(cache_l2.ways[0]->entries[1]->tag == 12);
    assert(cache_l2.ways[0]->entries[1]->is_valid);

    // fill L2 entry = 1 :
    cache_l2.cache_read(260); // way = 1 , tag = 32
    // now in L2 - entry = 1 is full - we will read miss in L2 and check invalidation of L1's data.
    // the LRU is -> way 0 -> tag = 12
    cache_l1.ways[1]->entries[1]->tag = 12; // artificial set , just for testing
    cache_l2.cache_read(164);

    assert(cache_l2.ways[0]->entries[1]->tag == 20);
    assert(cache_l2.ways[0]->entries[1]->is_valid);
    assert(cache_l1.ways[1]->entries[1]->tag == 12);
    assert(!cache_l1.ways[1]->entries[1]->is_valid);


    // sanity check for the right tree branch:
    cache_l1.cache_read(8);
    assert(cache_l1.ways[1]->entries[0]->tag == 1);
    assert(cache_l1.ways[1]->entries[0]->is_valid);
    assert(cache_l2.ways[1]->entries[0]->tag == 1);
    assert(cache_l2.ways[1]->entries[0]->is_valid);

}

void test_write_with_dirty_no_allocate(){

    Cache cache_l2(4,2,1,false,1,nullptr); // #entries = (2^4 / 2^2) / 2 = 2
    // #offset_bits = log2(block_size) = 2
    // # set_bits = log2(#entries) = 1

    Cache cache_l1(4,2,1,false,1,&cache_l2); // #entries = (2^4 / 2^2) / 2 = 2
    // #offset_bits = log2(block_size) = 2
    // # set_bits = log2(#entries) = 1

    cache_l2.set_upper_level_cache(&cache_l1);

    // write hit - simple:
    cache_l1.cache_read(100);
    cache_l1.cache_write(100);
    assert(cache_l1.ways[1]->entries[1]->tag == 12);
    assert(cache_l1.ways[1]->entries[1]->is_dirty);
    assert(cache_l1.ways[1]->entries[1]->is_valid);

    //write miss - no write allocate: assuring that we don't bring the address to the cache
    cache_l1.cache_write(32); //tag = 4
    assert(cache_l1.ways[1]->entries[0]->tag != 4);
    assert(!cache_l1.ways[1]->entries[0]->is_dirty);
    assert(!cache_l1.ways[1]->entries[0]->is_valid);
    assert(cache_l2.ways[1]->entries[0]->tag != 4);
    assert(!cache_l2.ways[1]->entries[0]->is_dirty);
    assert(!cache_l2.ways[1]->entries[0]->is_valid);


}

void test_write_with_dirty_allocate(){

    Cache cache_l2(4,2,1,true,1,nullptr); // #entries = (2^4 / 2^2) / 2 = 2
    // #offset_bits = log2(block_size) = 2
    // # set_bits = log2(#entries) = 1

    Cache cache_l1(4,2,1,true,1,&cache_l2); // #entries = (2^4 / 2^2) / 2 = 2
    // #offset_bits = log2(block_size) = 2
    // # set_bits = log2(#entries) = 1

    cache_l2.set_upper_level_cache(&cache_l1);

    // fill L1 both ways of entry = 1:
    cache_l1.ways[1]->entries[1]->tag = 12;
    cache_l1.ways[1]->entries[1]->is_valid = true;
    cache_l1.ways[0]->entries[1]->tag = 40;
    cache_l1.ways[0]->entries[1]->is_valid = true;

    // way = 1 will be LRU - we set dirty = true there:
    cache_l1.ways[1]->entries[1]->is_dirty = true;

    // simulate write miss for L1 cache - should call L2 read - way = 1 is LRU and dirty = true =>  should write it to L2.
    cache_l1.cache_write(68); // entry = 1, tag = 8
    assert(cache_l1.ways[1]->entries[1]->tag == 8);
    assert(cache_l1.ways[1]->entries[1]->is_valid);
    assert(cache_l1.ways[1]->entries[1]->is_dirty);
    // L2 assertions - the data should appear there according to the decisions tree
    assert(cache_l2.ways[0]->entries[1]->tag == 12);
    assert(cache_l2.ways[0]->entries[1]->is_valid);

    // test L2 write hit:
    cache_l2.cache_read(112);
    cache_l2.cache_write(112);
    assert(cache_l2.ways[1]->entries[0]->tag == 14);
    assert(cache_l2.ways[1]->entries[0]->is_dirty);
    assert(cache_l2.ways[1]->entries[0]->is_valid);


    // sanity check for the right tree branch:
    cache_l1.cache_write(8);
    assert(cache_l1.ways[1]->entries[0]->tag == 1);
    assert(cache_l1.ways[1]->entries[0]->is_valid);
    assert(cache_l2.ways[0]->entries[0]->tag == 1);
    assert(cache_l2.ways[0]->entries[0]->is_valid);


}

void test_read_miss_with_write_allocate_as_l1_when_lru_is_dirty_0() {   // all ways are occupied in L1
                                                                        // L1 has a dirty lru
                                                                        // not all ways are occupied in L2
                                                                        // L2 has wanted tag
                                                                        // L2 does have the wanted variable
                                                                        // L2 has the the lru evicted from L1
    uint32_t log_2_cache_size = 16;
    uint32_t log_2_block_size = 3;
    uint32_t log_2_ways = 2;
    uint32_t cycles = 1;
    bool is_write_allocate = true;

    Cache cache_l2 = Cache(log_2_cache_size, log_2_block_size, log_2_ways, is_write_allocate, cycles, nullptr);
    Cache cache_l1 = Cache(log_2_cache_size, log_2_block_size, log_2_ways, is_write_allocate, cycles, &cache_l2);

    uint32_t tag = 0;
    uint32_t dirty_tag = 24370;
    uint32_t address = 0;
    // we assume the current position of entry 0's lru_fsm is correct
    cache_l1.ways[0]->entries[0]->tag = 12387;
    cache_l1.ways[0]->entries[0]->is_valid = true;
    cache_l1.ways[1]->entries[0]->tag = 12387;
    cache_l1.ways[1]->entries[0]->is_valid = true;
    cache_l1.ways[2]->entries[0]->tag = 12387;
    cache_l1.ways[2]->entries[0]->is_valid = true;
    cache_l1.ways[3]->entries[0]->tag = dirty_tag;
    cache_l1.ways[3]->entries[0]->is_valid = true;
    cache_l1.ways[3]->entries[0]->is_dirty = true;

    cache_l2.ways[0]->entries[0]->tag = 12387;
    cache_l2.ways[0]->entries[0]->is_valid = true;
    cache_l2.ways[1]->entries[0]->tag = tag;
    cache_l2.ways[1]->entries[0]->is_valid = true;
    cache_l2.ways[2]->entries[0]->tag = dirty_tag;  // L2 has the dirty tag
    cache_l2.ways[2]->entries[0]->is_valid = true;
    cache_l2.ways[3]->entries[0]->tag = 0;
    cache_l2.ways[3]->entries[0]->is_valid = false; // not occupied

    cache_l1.cache_read(address);

    assert(cache_l1.ways[0]->entries[0]->tag == 12387);
    assert(cache_l1.ways[1]->entries[0]->tag == 12387);
    assert(cache_l1.ways[2]->entries[0]->tag == 12387);
    assert(cache_l1.ways[3]->entries[0]->tag == tag);

    assert(cache_l2.ways[0]->entries[0]->tag == 12387);
    assert(cache_l2.ways[1]->entries[0]->tag == tag);
    assert(cache_l2.ways[2]->entries[0]->tag == dirty_tag);
    assert(cache_l2.ways[2]->entries[0]->is_dirty == true);
    assert(cache_l2.ways[3]->entries[0]->tag == 0);
    assert(cache_l2.ways[3]->entries[0]->is_valid == false);
    assert(cache_l2.ways[3]->entries[0]->is_dirty == false);

    // checks that the lru fsm updated
    assert(cache_l1.lru_fsms[0]->ways_order_list.front() == 3);
    assert(cache_l1.lru_fsms[0]->ways_order_list.back() == 2);
    assert(cache_l2.lru_fsms[0]->ways_order_list.front() == 2);
    assert(cache_l2.lru_fsms[0]->ways_order_list.back() == 3);

    assert(cache_l1.total_misses == 1);
    assert(cache_l1.total_accesses == 1);
    assert(cache_l2.total_misses == 0);
    assert(cache_l2.total_accesses == 1);
    assert(cache_l2.total_main_memory_accesses == 0);
}

void test_read_miss_with_write_allocate_as_l1_when_lru_is_dirty_1() {   // all ways are occupied in L1
                                                                        // L1 has a dirty lru
                                                                        // not all ways are occupied in L2
                                                                        // L2 does have the wanted variable
                                                                        // L2 does not have L1's lru
    uint32_t log_2_cache_size = 16;
    uint32_t log_2_block_size = 3;
    uint32_t log_2_ways = 2;
    uint32_t cycles = 1;
    bool is_write_allocate = true;

    Cache cache_l2 = Cache(log_2_cache_size, log_2_block_size, log_2_ways, is_write_allocate, cycles, nullptr);
    Cache cache_l1 = Cache(log_2_cache_size, log_2_block_size, log_2_ways, is_write_allocate, cycles, &cache_l2);

    uint32_t tag = 0;
    uint32_t dirty_tag = 24370;
    uint32_t address = 0;
    // we assume the current position of entry 0's lru_fsm is correct
    cache_l1.ways[0]->entries[0]->tag = 12387;
    cache_l1.ways[0]->entries[0]->is_valid = true;
    cache_l1.ways[1]->entries[0]->tag = 12387;
    cache_l1.ways[1]->entries[0]->is_valid = true;
    cache_l1.ways[2]->entries[0]->tag = 12387;
    cache_l1.ways[2]->entries[0]->is_valid = true;
    cache_l1.ways[3]->entries[0]->tag = dirty_tag;
    cache_l1.ways[3]->entries[0]->is_valid = true;
    cache_l1.ways[3]->entries[0]->is_dirty = true;

    cache_l2.ways[0]->entries[0]->tag = 12387;
    cache_l2.ways[0]->entries[0]->is_valid = true;
    cache_l2.ways[1]->entries[0]->tag = tag;
    cache_l2.ways[1]->entries[0]->is_valid = true;
    cache_l2.ways[2]->entries[0]->tag = 12387;
    cache_l2.ways[2]->entries[0]->is_valid = true;
    cache_l2.ways[3]->entries[0]->tag = 0;
    cache_l2.ways[3]->entries[0]->is_valid = false; // not occupied

    cache_l1.cache_read(address);

    assert(cache_l1.ways[0]->entries[0]->tag == 12387);
    assert(cache_l1.ways[1]->entries[0]->tag == 12387);
    assert(cache_l1.ways[2]->entries[0]->tag == 12387);
    assert(cache_l1.ways[3]->entries[0]->tag == tag);   // he is supposed to be updated in L1 [was lru before the call]

    assert(cache_l2.ways[0]->entries[0]->tag == 12387);
    assert(cache_l2.ways[1]->entries[0]->tag == tag);
    assert(cache_l2.ways[2]->entries[0]->tag == 12387);
    assert(cache_l2.ways[3]->entries[0]->tag == dirty_tag);  // he is supposed to be the lru in L1
    assert(cache_l2.ways[3]->entries[0]->is_dirty == true);  // we updated the value of this in L2 after the WB, so we expect it to be dirty

    // checks that the lru fsm updated
    assert(cache_l1.lru_fsms[0]->ways_order_list.front() == 3);
    assert(cache_l1.lru_fsms[0]->ways_order_list.back() == 2);
    assert(cache_l2.lru_fsms[0]->ways_order_list.front() == 3);
    assert(cache_l2.lru_fsms[0]->ways_order_list.back() == 2);

    assert(cache_l1.total_misses == 1);
    assert(cache_l1.total_accesses == 1);
    assert(cache_l2.total_misses == 0);
    assert(cache_l2.total_accesses == 1);
    assert(cache_l2.total_main_memory_accesses == 0);
}

void test_read_miss_with_write_allocate_as_l1_when_lru_is_dirty_2() {   // all ways are occupied in L1
                                                                        // L1 has a dirty lru
                                                                        // all ways are occupied in L2
                                                                        // L2 does have the wanted variable
                                                                        // L2 does not have L1's lru

    uint32_t log_2_cache_size = 16;
    uint32_t log_2_block_size = 3;
    uint32_t log_2_ways = 2;
    uint32_t cycles = 1;
    bool is_write_allocate = true;

    Cache cache_l2 = Cache(log_2_cache_size, log_2_block_size, log_2_ways, is_write_allocate, cycles, nullptr);
    Cache cache_l1 = Cache(log_2_cache_size, log_2_block_size, log_2_ways, is_write_allocate, cycles, &cache_l2);

    uint32_t tag = 0;
    uint32_t dirty_tag = 24370;
    uint32_t address = 0;
    // we assume the current position of entry 0's lru_fsm is correct
    cache_l1.ways[0]->entries[0]->tag = 12387;
    cache_l1.ways[0]->entries[0]->is_valid = true;
    cache_l1.ways[1]->entries[0]->tag = 12387;
    cache_l1.ways[1]->entries[0]->is_valid = true;
    cache_l1.ways[2]->entries[0]->tag = 12387;
    cache_l1.ways[2]->entries[0]->is_valid = true;
    cache_l1.ways[3]->entries[0]->tag = dirty_tag;
    cache_l1.ways[3]->entries[0]->is_valid = true;
    cache_l1.ways[3]->entries[0]->is_dirty = true;

    cache_l2.ways[0]->entries[0]->tag = 12387;
    cache_l2.ways[0]->entries[0]->is_valid = true;
    cache_l2.ways[1]->entries[0]->tag = tag;
    cache_l2.ways[1]->entries[0]->is_valid = true;
    cache_l2.ways[2]->entries[0]->tag = 12387;
    cache_l2.ways[2]->entries[0]->is_valid = true;
    cache_l2.ways[3]->entries[0]->tag = dirty_tag;
    cache_l2.ways[3]->entries[0]->is_valid = true;

    cache_l1.cache_read(address);

    assert(cache_l1.ways[0]->entries[0]->tag == 12387);
    assert(cache_l1.ways[1]->entries[0]->tag == 12387);
    assert(cache_l1.ways[2]->entries[0]->tag == 12387);
    assert(cache_l1.ways[3]->entries[0]->tag == tag);   // he is supposed to be updated in L1 [was lru before the call]

    assert(cache_l2.ways[0]->entries[0]->tag == 12387);
    assert(cache_l2.ways[1]->entries[0]->tag == tag);
    assert(cache_l2.ways[2]->entries[0]->tag == 12387);
    assert(cache_l2.ways[3]->entries[0]->tag == dirty_tag);  // he is supposed to be the lru in L1
    assert(cache_l2.ways[3]->entries[0]->is_dirty == true);  // we updated the value of this in L2 after the WB, so we expect it to be dirty in L2

    // checks that the lru fsm updated
    assert(cache_l1.lru_fsms[0]->ways_order_list.front() == 3);
    assert(cache_l1.lru_fsms[0]->ways_order_list.back() == 2);
    assert(cache_l2.lru_fsms[0]->ways_order_list.front() == 3);
    assert(cache_l2.lru_fsms[0]->ways_order_list.back() == 2);

    assert(cache_l1.total_misses == 1);
    assert(cache_l1.total_accesses == 1);
    assert(cache_l2.total_misses == 0);
    assert(cache_l2.total_accesses == 1);
    assert(cache_l2.total_main_memory_accesses == 0);
}

void test_read_miss_with_write_allocate_as_l1_when_lru_is_dirty_3() {   // all ways are occupied in L1
                                                                        // L1 has a dirty lru
                                                                        // all ways are occupied in L2
                                                                        // L2 does have the wanted variable
                                                                        // L2 does not have the the lru of L1
                                                                        // L2 has a dirty lru
    uint32_t log_2_cache_size = 16;
    uint32_t log_2_block_size = 3;
    uint32_t log_2_ways = 2;
    uint32_t cycles = 1;
    bool is_write_allocate = true;

    Cache cache_l2 = Cache(log_2_cache_size, log_2_block_size, log_2_ways, is_write_allocate, cycles, nullptr);
    Cache cache_l1 = Cache(log_2_cache_size, log_2_block_size, log_2_ways, is_write_allocate, cycles, &cache_l2);

    uint32_t tag = 0;
    uint32_t dirty_tag = 24370;
    uint32_t dirty_tag_2 = 63721;
    uint32_t address = 0;
    // we assume the current position of entry 0's lru_fsm is correct
    cache_l1.ways[0]->entries[0]->tag = 12387;
    cache_l1.ways[0]->entries[0]->is_valid = true;
    cache_l1.ways[1]->entries[0]->tag = 12387;
    cache_l1.ways[1]->entries[0]->is_valid = true;
    cache_l1.ways[2]->entries[0]->tag = 12387;
    cache_l1.ways[2]->entries[0]->is_valid = true;
    cache_l1.ways[3]->entries[0]->tag = dirty_tag;
    cache_l1.ways[3]->entries[0]->is_valid = true;
    cache_l1.ways[3]->entries[0]->is_dirty = true;

    cache_l2.ways[0]->entries[0]->tag = dirty_tag;
    cache_l2.ways[0]->entries[0]->is_valid = true;
    cache_l2.ways[1]->entries[0]->tag = tag;
    cache_l2.ways[1]->entries[0]->is_valid = true;
    cache_l2.ways[2]->entries[0]->tag = 12387;
    cache_l2.ways[2]->entries[0]->is_valid = true;
    cache_l2.ways[3]->entries[0]->tag = dirty_tag_2;
    cache_l2.ways[3]->entries[0]->is_valid = true;
    cache_l2.ways[3]->entries[0]->is_dirty = true;

    cache_l1.cache_read(address);

    assert(cache_l1.ways[0]->entries[0]->tag == 12387);
    assert(cache_l1.ways[1]->entries[0]->tag == 12387);
    assert(cache_l1.ways[2]->entries[0]->tag == 12387);
    assert(cache_l1.ways[3]->entries[0]->tag == tag);   // he is supposed to be updated in L1 [was lru before the call]

    assert(cache_l2.ways[0]->entries[0]->tag == dirty_tag);
    assert(cache_l2.ways[1]->entries[0]->tag == tag);
    assert(cache_l2.ways[2]->entries[0]->tag == 12387);
    assert(cache_l2.ways[3]->entries[0]->tag == dirty_tag);  // he is supposed to be the lru in L1
    assert(cache_l2.ways[3]->entries[0]->is_dirty == true);  // we updated the value of this in L2 after the WB, so we expect it to be dirty

    // checks that the lru fsm updated
    assert(cache_l1.lru_fsms[0]->ways_order_list.front() == 3);
    assert(cache_l1.lru_fsms[0]->ways_order_list.back() == 2);
    assert(cache_l2.lru_fsms[0]->ways_order_list.front() == 3);
    assert(cache_l2.lru_fsms[0]->ways_order_list.back() == 2);

    assert(cache_l1.total_misses == 1);
    assert(cache_l1.total_accesses == 1);
    assert(cache_l2.total_misses == 0);
    assert(cache_l2.total_accesses == 1);
    assert(cache_l2.total_main_memory_accesses == 0);
}

// should act exactly the same to the equivalent test with write allocate (test_read_miss_with_write_allocate_as_l1_when_lru_is_dirty_0)
void test_read_miss_with_write_no_allocate_as_l1_when_lru_is_dirty_0() {   // all ways are occupied in L1
                                                                           // L1 has a dirty lru
                                                                           // not all ways are occupied in L2
                                                                           // L2 does have the wanted variable
                                                                           // L2 has L1's LRU
    uint32_t log_2_cache_size = 16;
    uint32_t log_2_block_size = 3;
    uint32_t log_2_ways = 2;
    uint32_t cycles = 1;
    bool is_write_allocate = false;

    Cache cache_l2 = Cache(log_2_cache_size, log_2_block_size, log_2_ways, is_write_allocate, cycles, nullptr);
    Cache cache_l1 = Cache(log_2_cache_size, log_2_block_size, log_2_ways, is_write_allocate, cycles, &cache_l2);

    uint32_t tag = 0;
    uint32_t dirty_tag = 24370;
    uint32_t address = 0;
    // we assume the current position of entry 0's lru_fsm is correct
    cache_l1.ways[0]->entries[0]->tag = 12387;
    cache_l1.ways[0]->entries[0]->is_valid = true;
    cache_l1.ways[1]->entries[0]->tag = 12387;
    cache_l1.ways[1]->entries[0]->is_valid = true;
    cache_l1.ways[2]->entries[0]->tag = 12387;
    cache_l1.ways[2]->entries[0]->is_valid = true;
    cache_l1.ways[3]->entries[0]->tag = dirty_tag;
    cache_l1.ways[3]->entries[0]->is_valid = true;
    cache_l1.ways[3]->entries[0]->is_dirty = true;

    cache_l2.ways[0]->entries[0]->tag = 12387;
    cache_l2.ways[0]->entries[0]->is_valid = true;
    cache_l2.ways[1]->entries[0]->tag = tag;
    cache_l2.ways[1]->entries[0]->is_valid = true;
    cache_l2.ways[2]->entries[0]->tag = dirty_tag;  // L2 has the dirty tag
    cache_l2.ways[2]->entries[0]->is_valid = true;
    cache_l2.ways[3]->entries[0]->tag = 0;
    cache_l2.ways[3]->entries[0]->is_valid = false; // not occupied

    cache_l1.cache_read(address);

    assert(cache_l1.ways[0]->entries[0]->tag == 12387);
    assert(cache_l1.ways[1]->entries[0]->tag == 12387);
    assert(cache_l1.ways[2]->entries[0]->tag == 12387);
    assert(cache_l1.ways[3]->entries[0]->tag == tag);

    assert(cache_l2.ways[0]->entries[0]->tag == 12387);
    assert(cache_l2.ways[1]->entries[0]->tag == tag);
    assert(cache_l2.ways[2]->entries[0]->tag == dirty_tag);
    assert(cache_l2.ways[2]->entries[0]->is_dirty == true);
    assert(cache_l2.ways[3]->entries[0]->tag == 0);
    assert(cache_l2.ways[3]->entries[0]->is_valid == false);
    assert(cache_l2.ways[3]->entries[0]->is_dirty == false);

    // checks that the lru fsm updated
    assert(cache_l1.lru_fsms[0]->ways_order_list.front() == 3);
    assert(cache_l1.lru_fsms[0]->ways_order_list.back() == 2);
    assert(cache_l2.lru_fsms[0]->ways_order_list.front() == 2);
    assert(cache_l2.lru_fsms[0]->ways_order_list.back() == 3);

    assert(cache_l1.total_misses == 1);
    assert(cache_l1.total_accesses == 1);
    assert(cache_l2.total_misses == 0);
    assert(cache_l2.total_accesses == 1);
    assert(cache_l2.total_main_memory_accesses == 0);
}

void test_read_miss_with_write_no_allocate_as_l1_when_lru_is_dirty_1() {   // all ways are occupied in L1
                                                                           // L1 has a dirty lru
                                                                           // not all ways are occupied in L2
                                                                           // L2 has the wanted variable
                                                                           // L2 does not have L1's LRU
    uint32_t log_2_cache_size = 16;
    uint32_t log_2_block_size = 3;
    uint32_t log_2_ways = 2;
    uint32_t cycles = 1;
    bool is_write_allocate = false;

    Cache cache_l2 = Cache(log_2_cache_size, log_2_block_size, log_2_ways, is_write_allocate, cycles, nullptr);
    Cache cache_l1 = Cache(log_2_cache_size, log_2_block_size, log_2_ways, is_write_allocate, cycles, &cache_l2);

    uint32_t tag = 0;
    uint32_t dirty_tag = 24370;
    uint32_t address = 0;
    // we assume the current position of entry 0's lru_fsm is correct
    cache_l1.ways[0]->entries[0]->tag = 12387;
    cache_l1.ways[0]->entries[0]->is_valid = true;
    cache_l1.ways[1]->entries[0]->tag = 12387;
    cache_l1.ways[1]->entries[0]->is_valid = true;
    cache_l1.ways[2]->entries[0]->tag = 12387;
    cache_l1.ways[2]->entries[0]->is_valid = true;
    cache_l1.ways[3]->entries[0]->tag = dirty_tag;
    cache_l1.ways[3]->entries[0]->is_valid = true;
    cache_l1.ways[3]->entries[0]->is_dirty = true;

    cache_l2.ways[0]->entries[0]->tag = 12387;
    cache_l2.ways[0]->entries[0]->is_valid = true;
    cache_l2.ways[1]->entries[0]->tag = tag;
    cache_l2.ways[1]->entries[0]->is_valid = true;
    cache_l2.ways[2]->entries[0]->tag = 12387;
    cache_l2.ways[2]->entries[0]->is_valid = true;
    cache_l2.ways[3]->entries[0]->tag = 0;
    cache_l2.ways[3]->entries[0]->is_valid = false; // not occupied

    cache_l1.cache_read(address);

    assert(cache_l1.ways[0]->entries[0]->tag == 12387);
    assert(cache_l1.ways[1]->entries[0]->tag == 12387);
    assert(cache_l1.ways[2]->entries[0]->tag == 12387);
    assert(cache_l1.ways[3]->entries[0]->tag == tag);   // he is supposed to be updated in L1 [was lru before the call]

    assert(cache_l2.ways[0]->entries[0]->tag == 12387);
    assert(cache_l2.ways[1]->entries[0]->tag == tag);
    assert(cache_l2.ways[2]->entries[0]->tag == 12387);
    assert(cache_l2.ways[3]->entries[0]->tag == 0);  // he is not supposed to be updated in L2 since it's write no allocate
    assert(cache_l2.ways[3]->entries[0]->is_valid == false);
    assert(cache_l2.ways[3]->entries[0]->is_dirty == false);

    // checks that the lru fsm updated
    assert(cache_l1.lru_fsms[0]->ways_order_list.front() == 3);
    assert(cache_l1.lru_fsms[0]->ways_order_list.back() == 2);
    assert(cache_l2.lru_fsms[0]->ways_order_list.front() == 1);
    assert(cache_l2.lru_fsms[0]->ways_order_list.back() == 3);

    assert(cache_l1.total_misses == 1);
    assert(cache_l1.total_accesses == 1);
    assert(cache_l2.total_misses == 0);
    assert(cache_l2.total_accesses == 1);
    assert(cache_l2.total_main_memory_accesses == 0);
}


void run_cache_tests() {

    test_create_lru_fsm();
    test_create_way_table();
    test_create_cache();
    test_LRU_update();
    test_read();
    test_read_miss_as_l1_1();
    test_read_miss_as_l1_2();
    test_read_miss_as_l1_3();
    test_read_miss_as_l1_4();
//    test_read_miss_as_l1_5();
    test_write_hit_1();
    test_write_hit_2();
    test_write_hit_3();
    test_write_miss_with_write_no_allocate_as_llc_1();
    test_write_miss_with_write_no_allocate_as_llc_2();
    test_write_miss_with_write_allocate_as_l1_1();
    test_write_miss_with_write_allocate_as_l1_2();
    test_write_miss_with_write_allocate_as_l1_3();
    test_write_miss_with_write_no_allocate_as_l1_1();
    test_write_miss_with_write_no_allocate_as_l1_2();
    test_write_miss_with_write_no_allocate_as_l1_3();

    test_write_statistics();
    test_generate_lru_address();
    test_lrufsm_invalidate();
    test_invalidate_block_1();
    test_invalidate_block_2();
    test_invalidate_block_3();
    test_invalidate_block_4();

    test_read_with_dirty();
    test_write_with_dirty_no_allocate();
    test_write_with_dirty_allocate();

    test_read_miss_with_write_allocate_as_l1_when_lru_is_dirty_0();
    test_read_miss_with_write_allocate_as_l1_when_lru_is_dirty_2();

    test_read_miss_with_write_no_allocate_as_l1_when_lru_is_dirty_0();

    cout << "\npassed all tests!" << endl;
}