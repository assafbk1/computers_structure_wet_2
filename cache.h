#ifndef HW_2_CACHE_H
#define HW_2_CACHE_H

#include <algorithm>
#include <vector>
#include <cstdint>
#include <list>
#include <cmath>
#include <iostream>
#include <cassert>

#define NO_TAG 0
#define INIT_SIZE 1
#define TOTAL_BITS 32

using std::list;

/** helper functions: **/



uint32_t create_mask(unsigned mask_size);


/** end of helper functions **/

class WayEntry {
public:
    uint32_t tag;
    bool is_valid;
    bool is_dirty;

    WayEntry(uint32_t tag, bool is_valid): tag(tag), is_valid(is_valid), is_dirty(false) {
    }

};

// a class for a single LRU FSM (one for each cache line)
class LruFsm {
public:
    uint32_t num_of_ways;
    list<uint32_t> ways_order_list;  // list size is the number of ways

    LruFsm(uint32_t num_of_ways): num_of_ways(num_of_ways), ways_order_list() {
        //init list:
        for (unsigned i=0;i<num_of_ways;i++){
            ways_order_list.push_back(i);
        }
    }

    // this function updates the LRU FSM
    // we change to place of way_num to be the first element (most recently used entry)
    // the return value is the last element (the least recently used entry)
    uint32_t update(uint32_t way_num){
        ways_order_list.remove(way_num);
        ways_order_list.push_front(way_num);
        return ways_order_list.back();
    }
    // function returns the LRU way - the last element on the list
    uint32_t get_lru_way(){
        return ways_order_list.back();
    }

    uint32_t invalidate(uint32_t way_num){
        ways_order_list.remove(way_num);
        ways_order_list.push_back(way_num);
        return ways_order_list.back();
    }

    // no need for d'tor, the list will destruct itself.
};

// a class for a single way table inside the cache
class Way {
public:
    uint32_t num_of_entries;
    std::vector<WayEntry*> entries;

    Way(uint32_t num_of_entries): num_of_entries(num_of_entries), entries(num_of_entries) {

        // init entries in way to null value
        for (int i=0; i< this->num_of_entries; i++) {
            entries[i] = new WayEntry(NO_TAG, false);
        }
    }

    ~Way() {
        // init entries in way to null value
        for (int i=0; i< num_of_entries; i++) {
            delete entries[i];
        }
    }

};

class Cache {
public:
    std::vector<Way*> ways;
    std::vector<LruFsm*> lru_fsms;
    bool is_write_allocate;
    Cache* lower_level_cache; // the next cache in the hierarchy (under this one). if this is the LLC this should be set to nullptr
    Cache* upper_level_cache; // if this is the L1 cache this should be set to nullptr

    // metadata
    uint32_t cache_size; // in bytes
    uint32_t num_of_ways;
    uint32_t num_of_entries;
    uint32_t block_size; // used for calculating the num of offset bits.

    // statistics
    uint32_t cycles_per_access;
    uint32_t total_misses;
    uint32_t total_accesses;
    uint32_t total_access_time;  // total access time in cycles
    uint32_t total_main_memory_accesses; // should be untouched if this cache is not LLC.

    //uint32_t total_misses_l2; NOT NEEDED
    //uint32_t total_accesses_to_l2; NOT NEEDED

    // we don't define total accesses to memory because it is equal to total accesses to l1

    Cache(uint32_t log_2_cache_size, uint32_t log_2_block_size, uint32_t log_2_ways, bool is_write_allocate, uint32_t cycles_per_access, Cache* next_level_cache):
                                                                             cache_size(uint32_t(pow(2,log_2_cache_size))),
                                                                             is_write_allocate(is_write_allocate),
                                                                             num_of_ways(uint32_t(pow(2,log_2_ways))),
                                                                             ways(INIT_SIZE),
                                                                             block_size(uint32_t(pow(2,log_2_block_size))),

                                                                             /*
                                                                              * num of entries calculation:
                                                                              *   single_way_table_size = cache_size/num_of_ways
                                                                              *   number_of_entries_in_single_way_table = single_way_table_size/block_size
                                                                              *   number_of_entries_in_cache = number_of_entries_in_single_way_table
                                                                              */
                                                                             num_of_entries(uint32_t(pow(2, ( log_2_cache_size - log_2_block_size - log_2_ways )))),
                                                                             lru_fsms(INIT_SIZE),
                                                                             cycles_per_access(cycles_per_access),
                                                                             total_misses(0),
                                                                             total_accesses(0),
                                                                             total_access_time(0),
                                                                             total_main_memory_accesses(0),
                                                                             lower_level_cache(next_level_cache),
                                                                             upper_level_cache(nullptr) // set this using set_upper_level_cache
                                                                             {

        ways.resize(num_of_ways);
        lru_fsms.resize(num_of_entries);

        // init all LRU FSMs
        for (int i=0; i < num_of_entries; i++) {
            lru_fsms[i] = new LruFsm(num_of_ways);
        }

        // init all ways
        for (int i=0; i < num_of_ways; i++) {
            ways[i] = new Way(num_of_entries);
        }
    }

    ~Cache() {

        for (int i=0; i < num_of_entries; i++) {
            delete lru_fsms[i];
        }

        for (int i=0; i < num_of_ways; i++) {
            delete ways[i];
        }
    }

    bool cache_read(uint32_t address, bool is_wb=false){

        // calculate entry
        uint32_t cache_entry = address;
        uint32_t num_of_offset_bits = uint32_t (log2(this->block_size)); // block size is a power of 2.
        cache_entry = cache_entry >> num_of_offset_bits; // getting rid of the offset bits
        cache_entry = cache_entry & create_mask(unsigned(log2(this->num_of_entries))); // num_of_entries is a power of 2.

        // calculate tag
        uint32_t cache_tag = address;
        cache_tag = cache_tag >> (num_of_offset_bits + unsigned(log2(this->num_of_entries))); // getting rid of the offset + set bits
        cache_tag = cache_tag & create_mask(TOTAL_BITS - (num_of_offset_bits + unsigned(log2(this->num_of_entries)))); //mask with the num of tag bits

        // check if tag exists in one of the ways
        bool hit = false;
        uint32_t num_way_hit = 0; // the 0 meaningless, only for init
        for(unsigned i=0;i<num_of_ways;i++){
            if(ways[i]->entries[cache_entry]->tag == cache_tag && ways[i]->entries[cache_entry]->is_valid){
                hit = true;
                num_way_hit = i;
            }
        }

        if(hit){ // the tag exists in way number: num_way_hit

            // 1. update LRU FSM
            lru_fsms[cache_entry]->update(num_way_hit);

        } else { // miss

            // simulate read of this variable in next level cache.
            // if this is the LLC we add an access to the main memory statistics
            if (this->lower_level_cache) {
                this->lower_level_cache->cache_read(address, is_wb);
            } else {
                if (!is_wb) {
                    this->total_main_memory_accesses++;
                }
            }

        // new block "arrived", we need to insert it to the wanted entry.

            // first check if lru block is dirty and valid, if so perform a writeback [invalidate upper level and wb to lower level]
            uint32_t lru_way = lru_fsms[cache_entry]->get_lru_way();

            // calculate LRU address
            uint32_t lru_block_addr = get_lru_address(ways[lru_way]->entries[cache_entry]->tag, cache_entry);

            if (ways[lru_way]->entries[cache_entry]->is_valid && this->upper_level_cache) { // invalidate LRU- only if we threw something valid
                this->upper_level_cache->invalidate_block(lru_block_addr);
            }

            if (ways[lru_way]->entries[cache_entry]->is_valid && ways[lru_way]->entries[cache_entry]->is_dirty) {

                // writeback to next level
                if (this->lower_level_cache) {
                    this->lower_level_cache->cache_write(lru_block_addr, true);
                } // no need to update statistics of main memory on wb


                ways[lru_way]->entries[cache_entry]->is_dirty = false; // unset the dirty bit
            }

            // insert the new block to the entry
            ways[lru_way]->entries[cache_entry]->tag = cache_tag; // insert tag to the LRU way.
            ways[lru_way]->entries[cache_entry]->is_valid = true; // update block status to valid

            //update LRU FSM
            lru_fsms[cache_entry]->update(lru_fsms[cache_entry]->get_lru_way());

        }

        // update statistics:
        if (!is_wb) {
            update_statistics(hit);
        }

        return hit;
    }

    bool cache_write(uint32_t address, bool is_wb=false){

        // calculate entry (set value)
        uint32_t cache_entry = address;
        uint32_t num_of_offset_bits = uint32_t (log2(this->block_size)); // block size is a power of 2.
        cache_entry = cache_entry >> num_of_offset_bits; // getting rid of the offset bits
        cache_entry = cache_entry & create_mask(unsigned(log2(this->num_of_entries))); // num_of_entries is a power of 2.

        // calculate tag
        uint32_t cache_tag = address;
        cache_tag = cache_tag >> (num_of_offset_bits + unsigned(log2(this->num_of_entries))); // getting rid of the offset + set bits
        cache_tag = cache_tag & create_mask(TOTAL_BITS - (num_of_offset_bits + unsigned(log2(this->num_of_entries)))); //mask with the num of tag bits

        // check if tag exists in one of the ways
        bool hit = false;
        uint32_t num_way_hit = 0; // the 0 is meaningless, only for init
        for(unsigned i=0;i<num_of_ways;i++){
            if(ways[i]->entries[cache_entry]->tag == cache_tag && ways[i]->entries[cache_entry]->is_valid){
                hit = true;
                num_way_hit = i;
            }
        }

        if (is_wb) {
            assert(hit==true); // we assert that the variable should be in this level (from the inclusion principal)
        }

        if(hit){ // the tag exists in way number: num_way_hit

            // 1. update LRU FSM
            lru_fsms[cache_entry]->update(num_way_hit);

            // 2. set dirty bit
            ways[num_way_hit]->entries[cache_entry]->is_dirty = true;

        } else { // miss

            if (this->is_write_allocate) {

                // we can't be here with L2 cache - assertion
                // assert(this->lower_level_cache != nullptr); FIXME - enable it when running external tests

                // simulate read of this variable in next level cache.
                // if this is the LLC we add an access to the main memory statistics
                if (this->lower_level_cache) {
                  this->lower_level_cache->cache_read(address, is_wb);
                } else {
                    if (!is_wb) {
                        this->total_main_memory_accesses++;
                    }
                }

                //  update LRU tag
                uint32_t lru_way = lru_fsms[cache_entry]->get_lru_way();

                // calculate LRU address
                uint32_t lru_block_addr = get_lru_address(ways[lru_way]->entries[cache_entry]->tag, cache_entry);

                // invalidate LRU- only if we threw something valid
                if (ways[lru_way]->entries[cache_entry]->is_valid && this->upper_level_cache) {
                    this->upper_level_cache->invalidate_block(lru_block_addr);
                }

                // perform writeback if we threw something valid and dirty
                if (ways[lru_way]->entries[cache_entry]->is_valid && ways[lru_way]->entries[cache_entry]->is_dirty) {

                    // writeback to next level
                    if (this->lower_level_cache) {
                        this->lower_level_cache->cache_write(lru_block_addr, true);
                    } // no need to update statistics of main memory on wb


                    ways[lru_way]->entries[cache_entry]->is_dirty = false; // unset the dirty bit - FIXME - redundant?
                }

                // insert the new block to the entry
                ways[lru_way]->entries[cache_entry]->tag = cache_tag; // insert tag to the LRU way.
                ways[lru_way]->entries[cache_entry]->is_valid = true; // update block status to valid
                ways[lru_way]->entries[cache_entry]->is_dirty = true; // set dirty bit

                //update LRU FSM
                lru_fsms[cache_entry]->update(lru_fsms[cache_entry]->get_lru_way());


            } else {

                // simulate write of this variable in next level cache.
                // if this is the LLC we add an access to the main memory statistics
                if (this->lower_level_cache) {
                    this->lower_level_cache->cache_write(address, is_wb);
                } else {
                    if (!is_wb) {
                        this->total_main_memory_accesses++;
                    }
                }

            }
        }

        // FIXME - didn't add mem_cycles yet.

        // update statistics:
        if (!is_wb) {
            update_statistics(hit);
        }
    }

    void update_statistics(bool hit){

         total_accesses++;
         total_access_time += cycles_per_access;
         if(!hit) total_misses++;

    }

    uint32_t get_lru_address(uint32_t tag, uint32_t entry) {

        uint32_t num_of_offset_bits = uint32_t(log2(this->block_size));
        uint32_t num_of_entry_bits = unsigned(log2(this->num_of_entries));
        uint32_t address = tag << num_of_entry_bits;
        address = (address + entry) << num_of_offset_bits;
        return address;
    }

    void set_upper_level_cache(Cache* upper_level_cache) {
        this->upper_level_cache = upper_level_cache;
    }

    void invalidate_block(uint32_t block_address) {

        // calculate entry
        uint32_t cache_entry = block_address;
        uint32_t num_of_offset_bits = uint32_t (log2(this->block_size)); // block size is a power of 2.
        cache_entry = cache_entry >> num_of_offset_bits; // getting rid of the offset bits
        cache_entry = cache_entry & create_mask(unsigned(log2(this->num_of_entries))); // num_of_entries is a power of 2.

        // calculate tag
        uint32_t cache_tag = block_address;
        cache_tag = cache_tag >> (num_of_offset_bits + unsigned(log2(this->num_of_entries))); // getting rid of the offset + set bits
        cache_tag = cache_tag & create_mask(TOTAL_BITS - (num_of_offset_bits + unsigned(log2(this->num_of_entries)))); //mask with the num of tag bits

        // check if tag exists in one of the ways
        uint32_t num_way_hit = 0; // the 0 meaningless, only for init
        for(unsigned i=0;i<num_of_ways;i++){
            if(this->ways[i]->entries[cache_entry]->tag == cache_tag && this->ways[i]->entries[cache_entry]->is_valid){
                this->ways[i]->entries[cache_entry]->is_valid = false;
                this->ways[i]->entries[cache_entry]->is_dirty = false;
                this->lru_fsms[cache_entry]->invalidate(i);
            }
        }
    }

};


#endif //HW_2_CACHE_H
